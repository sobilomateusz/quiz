package personal_projects.quiz.frontend;

import lombok.Data;

@Data
public class UserAnswer {
    private String answer;
}
