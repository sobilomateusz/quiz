package personal_projects.quiz.frontend;

public enum Difficulty {
    EASY,
    MEDIUM,
    HARD
}
