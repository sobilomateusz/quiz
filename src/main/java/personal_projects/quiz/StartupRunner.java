package personal_projects.quiz;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import personal_projects.quiz.database.entities.PlayerEntity;
import personal_projects.quiz.database.repositories.PlayerRepository;
import personal_projects.quiz.services.QuizDataService;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Component
@Log
public class StartupRunner implements CommandLineRunner {

    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private QuizDataService quizDataService;

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        log.info("Executing startup actions...");

        playerRepository.save(new PlayerEntity("John"));
        playerRepository.save(new PlayerEntity("Harry"));
        playerRepository.save(new PlayerEntity("George"));

        log.info("List of players in database:");
        List<PlayerEntity> playersFromDb = playerRepository.findAll();
        for (PlayerEntity player : playersFromDb) {
            log.info("Retrieved player: " + player);
        }

        quizDataService.getQuizCategories();
    }
}
